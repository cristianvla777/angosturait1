			
			// establecer las variables que tienen que ver con las fechas y el tiempo en días transcurridos
			var fechaInicial= new Date();
				fechaInicial.setHours(0,0,0,0,0);
				fechaInicial.setDate(13);
				fechaInicial.setMonth(7);
				fechaInicial.setYear(2017);
				var fechaHoy=new Date();
				var fechaUsuario=document.getElementById("dia").value;
				var fechaFutura= new Date(fechaUsuario);
				var dia=document.getElementById("dia").value;
				var fechaF=Date.parse(fechaFutura);
				var fecha=new Date(fechaF);
				fechaFutura.setHours(0,0,0,0);
				var lapso_a_hoy=parseInt((fechaHoy.getTime()-fechaInicial.getTime())/86400000);
				var lapso_a_futuro=parseInt((fechaFutura.getTime()-fechaInicial.getTime())/86400000);
				turnoActual=document.getElementById("turno").value;
				//la variable que crea el ciclo o sea el cronograma, cada pareja tiene un lugar unico y de distancias constantes con respecto a los demas, el ciclo tiene 144 días:                                     
				var ciclo144=["FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","FL","4","4","4","4","4","4","F","3","3","3","3","3","3","F","2","2","2","2","2","2","F","1","1","1","1","1","1","F","F","F","4","4","4","4","4","4","F","3","3","3","3","3","3","F","2","2","2","2","2","2","F","1","1","1","1","1","1","F","F","F","4","4","4","4","4","4","F","3","3","3","3","3","3","F","2","2","2","2","2","2","F","1","1","1","1","1","1","F","F","F","4","4","4","4","4","4","F","3","3","3","3","3","3","F","2","2","2","2","2","2","F","1","1","1","1","1","1","F","F","F","4","4","4","4","4","4","F","3","3","3","3","3","3","F","2","2","2","2","2","2","F","1","1","1","1","1","1","F","F","F"];
				//variables que inicializan el estado de cada pareja, tomando como referencia una fecha(declarada anteriormente) en la que inició el ciclo
				var indiceTurno=0;
				//la variable moduloAfuturo es importante porque me dará el punto del ciclo donde se encontrará cada turno
				var moduloApasado=lapso_a_hoy%144;
				var calTurnoHoy=143-moduloApasado;
				var moduloAfuturo=lapso_a_futuro%144;
				var indiceA=0;
				var indiceB=24;
				var indiceC=48;
				var indiceD=72;
				var indiceE=96;
				var indiceF=120;
				//Destina los nombres para una mejor referencia de quien es quien en cada turno
				var VeraD_Smith=ciclo144[indiceA];
				var VeraM_Riffo=ciclo144[indiceB];
				var Bosse_Passera=ciclo144[indiceC];
				var OrellanaC_Valdebenito=ciclo144[indiceD];
				var Cabella_OrellanaH=ciclo144[indiceE];
				var Has_Cardenas=ciclo144[indiceF];
				//define la función principal, que será llamada desde el evento onCLick del boton "calcular"
					function correrCronograma(){
						// establecer variables a su estado inicial para evitar errores por calculos anteriores
						indiceTurno=0;
	 					moduloApasado=lapso_a_hoy%144;
	 					calTurnoHoy=143-moduloApasado;
				 		moduloAfuturo=lapso_a_futuro%144;
				 		indiceA=0;
				 		indiceB=24;
				 		indiceC=48;
						indiceD=72;
						indiceE=96;
						indiceF=120;
				 		VeraD_Smith=ciclo144[indiceA];
				 		VeraM_Riffo=ciclo144[indiceB];
				 		Bosse_Passera=ciclo144[indiceC];
						OrellanaC_Valdebenito=ciclo144[indiceD];
						Cabella_OrellanaH=ciclo144[indiceE];
						Has_Cardenas=ciclo144[indiceF];
					//definición de las funciones anidadas, una por cada turno o pareja
						function correrCronogramaA(){
												
												for(i=moduloAfuturo; i>0; i--)
												{
												indiceA++;
													if(indiceA==144){
																	indiceA=0;
																	}


												}
												VeraD_Smith=ciclo144[indiceA];
												}
						function correrCronogramaB()
												{
												
												for(i=moduloAfuturo; i>0; i--)
												{
												indiceB++;
													if(indiceB==144){
																	indiceB=0;
																	}


												}
												VeraM_Riffo=ciclo144[indiceB];
												}
						function correrCronogramaC()
												{
											
												for(i=moduloAfuturo; i>0; i--)
												{
												indiceC++;
													if(indiceC==144){
																	indiceC=0;
																	}


												}
												Bosse_Passera=ciclo144[indiceC];
												}
						function correrCronogramaD()
												{
												
												for(i=moduloAfuturo; i>0; i--)
												{
												indiceD++;
													if(indiceD==144){
																	indiceD=0;
																	}


												}
												OrellanaC_Valdebenito=ciclo144[indiceD];
												}
						function correrCronogramaE()
												{
												for(i=moduloAfuturo; i>0; i--)
												{
												indiceE++;
												if (indiceE==144)	{
																	indiceE=0;
																	}
												}
												Cabella_OrellanaH=ciclo144[indiceE];					
											
												}
												
						function correrCronogramaF()
												{
												for(i=moduloAfuturo; i>0; i--)
												{
												indiceF++;
													if(indiceF==144){
																	indiceF=0;
																	}


												}
												Has_Cardenas=ciclo144[indiceF];
												}
						//llamado a las funciones anidadas(para que sean efectivamente ejecutadas
						correrCronogramaA()
						correrCronogramaB()
						correrCronogramaC()
						correrCronogramaD()
						correrCronogramaE()
						correrCronogramaF()
						//la salida del programa estará relacionada con la pareja seleccionada por el usuario

						switch (turnoActual){
											case "Z":
												document.getElementById('resultado').innerHTML="Orellana C - Valdebenito<br>"+"Turno:"+OrellanaC_Valdebenito;
											break;
											case "Y":
												document.getElementById('resultado').innerHTML="Riffo - Vera M <br>"+"Turno:"+VeraM_Riffo;
											break;
											case "X":
												document.getElementById('resultado').innerHTML="Bosse - Passera<br> "+"Turno:"+Bosse_Passera;
											break;
											case "W":
												document.getElementById('resultado').innerHTML="Has - Cardenas<br> "+"Turno:"+Has_Cardenas;
											break;
											case "V":
												document.getElementById('resultado').innerHTML="Orellana H - Cabella<br> "+"Turno:"+Cabella_OrellanaH;
											break;
											case "U":
												document.getElementById('resultado').innerHTML="Vera D - Smith<br> "+"Turno:"+VeraD_Smith;
											break;
											}

								}